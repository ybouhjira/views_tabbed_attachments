<?php ob_start(); ?>

<?php
// REMOVING view-dom-id-... from classes %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$classes_array = explode(" ", $classes);
$classes = "";
$view_dom_id = "";
foreach ($classes_array as $class) {
    if (!preg_match('/^view-dom-id/', $class)) {
        $classes .= "$class ";
    } else {
        $view_dom_id = $class;
    }
}
?>

<?php // VIEW OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ?>
<div class="<?php print $classes; ?>">
    <?php print render($title_prefix); ?>
    <?php if ($title): ?>
        <?php print $title; ?>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($header): ?>
        <div class="view-header"> <?php print $header; ?> </div>
    <?php endif; ?>
    <?php if ($rows): ?>
        <div class="view-content"> <?php print $rows; ?> </div>
    <?php elseif ($empty): ?>
        <div class="view-empty"> <?php print $empty; ?> </div>
    <?php endif; ?>
    <?php if ($pager): ?>
        <?php print $pager; ?>
    <?php endif; ?>
    <?php if ($more): ?>
        <?php print $more; ?>
    <?php endif; ?>
    <?php if ($footer): ?>
        <div class="view-footer"> <?php print $footer; ?> </div>
    <?php endif; ?>
    <?php if ($feed_icon): ?>
        <div class="feed-icon"> <?php print $feed_icon; ?> </div>
    <?php endif; ?>
</div>

<?php
// PUT THE OUTPUT IN $view_output VARIABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$view_output = ob_get_contents();
ob_clean();
?>


<? if ($view->is_attachment) : ?>
    <div id="tab-<? print $view->current_display ?>">
        <? echo $view_output ?>
    </div>

<? elseif (!empty($attachment_before) || !empty($attachment_after)) : ?>
    <? drupal_add_library('system', 'ui.tabs'); ?>

    <?php $wrapper_id = 'tabs-' . $view->current_display . '-wrapper'; ?>
    <div class="<? echo $view_dom_id; ?>">
        <div id="<? echo $wrapper_id; ?>" >

            <? /* ================= EXPOSED FILTERS FORM ================= */ ?>
            <div class="view-filters">
                <?php print $exposed; ?>
            </div>

            <? /* ==================== The tabs' UL ====================== */ ?>
            <ul>
                <? foreach ($view->display as $display_id => $display) : ?>
                    <? if ($display_id != 'default') : ?>
                        <li>
                            <a href='#tab-<? echo $display_id; ?>'>
                                <? echo $display->display_title; ?>
                            </a>
                        </li>
                    <? endif; ?>
                <? endforeach; ?>
            </ul>

            <? echo $attachment_before; ?>

            <? /* ============= THE MAIN DISPLAY ========================== */ ?>
            <div id="tab-<? print $view->current_display ?>">
                <? echo $view_output ?>
            </div>

            <? echo $attachment_after; ?>

            <? /* Put the script that triggers jQueryUI tabs */ ?>
            <?php if (!preg_match('#^admin/structure/views/#', $_GET['q'])) : ?>
                <script>
                    (function($) {
                        Drupal.behaviors.views_tabbed_attachments = {
                            attach: function() {
                                var wrapper = $('#<? echo $wrapper_id; ?>');
                                wrapper.tabs();
                                wrapper.tabs('select', window.views_tabbed_current_tab_id);
                                wrapper.find('ul.ui-tabs-nav li a').click(function(event) {
                                    window.views_tabbed_current_tab_id = $(this).attr('href');
                                });
                            }
                        };

                    })(jQuery);

                </script>
            <? endif; ?>
        </div>
    </div>
<? endif; ?>
